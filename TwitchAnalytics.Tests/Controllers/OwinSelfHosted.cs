using System.Web.Http;
using Owin;

namespace TwitchAnalytics.Controllers

{
    public class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config); // Call the WebApiConfig.Register method to configure Web API routes
            appBuilder.UseWebApi(config);
        }
    }
}   