﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using TwitchAnalytics;
using TwitchAnalytics.Controllers;

namespace TwitchAnalytics.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Disponer
            HomeController controller = new HomeController();

            // Actuar
            ViewResult result = controller.Index() as ViewResult;

            // Declarar
            Assert.IsNotNull(result);
            Assert.AreEqual("Home Page", result.ViewBag.Title);
        }
    }
}
