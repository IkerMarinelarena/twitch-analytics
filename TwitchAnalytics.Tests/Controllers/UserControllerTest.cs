using System.Threading.Tasks;
using System.Web.Helpers;
using Microsoft.Owin.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using NUnit.Framework;
using TwitchAnalytics.Controllers;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace TwitchAnalytics.Tests.Controllers
{
    [TestClass]
    public class UserControllerTest
    {
        [Test]
        public async Task GetsUserData()
        {
            using (var server = TestServer.Create<Startup>())
            {
                var getUserResponse = await server.HttpClient.GetAsync("/api/user/1");
                string userData = await getUserResponse.Content.ReadAsStringAsync();

                string expectedUserData =
                    "{\"data\":[{\"id\":\"1\",\"login\":\"elsmurfoz\",\"display_name\":\"elsmurfoz\",\"type\":\"\",\"broadcaster_type\":\"\",\"description\":\"\",\"profile_image_url\":\"https://static-cdn.jtvnw.net/user-default-pictures-uv/215b7342-def9-11e9-9a66-784f43822e80-profile_image-300x300.png\",\"offline_image_url\":\"\",\"view_count\":0,\"created_at\":\"2007-05-22T10:37:47Z\"}]}";

                Assert.AreEqual(expectedUserData, userData);
            }
        }
    }
}
