namespace TwitchAnalytics.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Http;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    public class UserController : ApiController
    {
        private static readonly HttpClient HttpClient = new HttpClient();

        public async Task<IHttpActionResult> GetUserData(string id)
        {
            try
            {
                var tokenResponse = await GetAccessToken();
                var accessToken = tokenResponse["access_token"].ToString();
                var userDataResponse = await GetUserData(accessToken, id);

                return Ok(userDataResponse);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        private async Task<JObject> GetAccessToken()
        {
            const string clientId = "vif1fjvzofgrsxw86fojnvyzijt8pk";
            const string clientSecret = "vbr42fnewmyoarpo6melafetq5ky7j";

            var requestData = new FormUrlEncodedContent(
                new[]
                {
                    new KeyValuePair<string, string>("client_id", clientId),
                    new KeyValuePair<string, string>("client_secret", clientSecret),
                    new KeyValuePair<string, string>("grant_type", "client_credentials"),
                }
            );

            var response = await HttpClient.PostAsync("https://id.twitch.tv/oauth2/token", requestData);
            response.EnsureSuccessStatusCode();

            var responseContent = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<JObject>(responseContent);
        }

        private async Task<JObject> GetUserData(string accessToken, string id)
        {
            HttpClient.DefaultRequestHeaders.Clear();
            HttpClient.DefaultRequestHeaders.Add("Client-ID", "vif1fjvzofgrsxw86fojnvyzijt8pk");
            HttpClient.DefaultRequestHeaders.Add("Authorization", $"Bearer {accessToken}");

            var response = await HttpClient.GetAsync($"https://api.twitch.tv/helix/users?id={id}");
            response.EnsureSuccessStatusCode();

            var responseContent = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<JObject>(responseContent);
        }
    }
}
